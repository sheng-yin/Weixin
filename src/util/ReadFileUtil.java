package util;

import java.io.BufferedReader;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
//import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadFileUtil {
	
	public static String readFile(String fileName){
		
		StringBuffer sb = new StringBuffer();
//		File file = new File(fileName);
		BufferedReader br = null;
		try {
			FileInputStream file = new FileInputStream(fileName);
//			FileReader fr = new FileReader(file);
			InputStreamReader input = new InputStreamReader(file,"utf-8");
			br = new BufferedReader(input); //构造一个BufferedReader类来读取文件
			String temstring = null;
			while((temstring = br.readLine()) != null){
				sb.append(temstring+System.lineSeparator());
//				sb.append(System.lineSeparator()+temstring);
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(br != null){
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
//		System.out.println("----->" + sb.toString());
		return sb.toString();

	}
	
	
//	public static void main(String[] args){
//		System.out.println(readFile("WebContent/TXT/test.txt"));
//		System.out.println(System.getProperty("user.dir"));
//	}
	

}
