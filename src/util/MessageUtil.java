package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.thoughtworks.xstream.XStream;

import po.Image;
import po.ImageMessage;
import po.Music;
import po.MusicMessage;
import po.News;
import po.NewsMessage;
import po.TextMessage;
import po.Voice;
import po.VoiceMessage;

/**
 * 主消息工具类
 * @author shengyin
 *
 */
public class MessageUtil {
	
	public static final String MESSAGE_TEXT = "text";
	public static final String MESSAGE_NEWS = "news";
	public static final String MESSAGE_IMAGE = "image";
	public static final String MESSAGE_MUSIC = "music";
	public static final String MESSAGE_THUMB = "thumb";
	public static final String MESSAGE_VOICE = "voice";
	public static final String MESSAGE_VIDEO = "video";
	public static final String MESSAGE_LINK = "link";
	public static final String MESSAGE_LOCATION = "location";
	public static final String MESSAGE_SCANCODE_WAITMSG = "scancode_waitmsg";
	public static final String MESSAGE_EVENT = "event";
	public static final String MESSAGE_SUBSCRIBE = "subscribe";
	public static final String MESSAGE_UNSUBSCRIBE = "unsubscribe";
	public static final String MESSAGE_CLICK = "CLICK";
	public static final String MESSAGE_VIEW = "VIEW";
	//临时图片素材path
//	public static final String IMAGE_PATH="G:/JG.png";
//	public static final String MUSIC_PATH="G:/m.jpg";
//	public static final String VOICE_PATH="G:/ly.mp3";
	
	
	/**
	 * 将XML转换成map集合
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws DocumentException
	 */
	public static Map<String, String> xmlToMap(HttpServletRequest request) throws IOException, DocumentException{
		//创建集合对象
		Map<String, String> map = new HashMap<String, String>();
		SAXReader reader = new SAXReader();
		//获取输入流
		InputStream ins = request.getInputStream();
		Document doc = reader.read(ins);
		//读取ROOT节点
		Element root = doc.getRootElement();
		//读取的节点放入集合中			
		List<Element> list = root.elements();
		//遍历集合对象
		for(Element e:list){
			map.put(e.getName(), e.getText());
		}
		ins.close();
		System.out.println("获取的微信响应map：" + map);
		return map;
		
	}
	
	/**
	 * 将文本消息对象转换成XML
	 * @param textMessage
	 * @return
	 */
	public static String textMessageToXml(TextMessage textMessage){
		XStream xstream = new XStream();
		xstream.alias("xml", textMessage.getClass());
		return xstream.toXML(textMessage);
	}
	
	
	/**
	 * 回复图文消息转为XML
	 * @param textMessage
	 * @return 转成XML格式消息
	 */
	public static String newsMessageToXml(NewsMessage newsMessage){
		XStream xstream = new XStream();
		xstream.alias("xml", newsMessage.getClass());
		xstream.alias("item", new News().getClass());
		return xstream.toXML(newsMessage);
	}
	
	/**
	 * 回复图片消息转为XML
	 * @param imageMessage
	 * @return
	 */
	public static String imageMessageToXml(ImageMessage imageMessage){
		XStream xstream = new XStream();
		xstream.alias("xml", imageMessage.getClass());
		xstream.alias("Image", new Image().getClass());
		return xstream.toXML(imageMessage);
	}
	
	
	/**
	 * 回复语音消息转为XML
	 * @param voiceMessage
	 * @return
	 */
	public static String voiceMessageToXml(VoiceMessage voiceMessage){
		XStream xstream = new XStream();
		xstream.alias("xml", voiceMessage.getClass());
		xstream.alias("Voice", new Voice().getClass());
		return xstream.toXML(voiceMessage);
	}
	
	/**
	 * 回复音乐消息转为XML
	 * @param musicMessage
	 * @return
	 */
	public static String musicMessageToXml(MusicMessage musicMessage){
		XStream xstream = new XStream();
		xstream.alias("xml", musicMessage.getClass());
		xstream.alias("Music", new Music().getClass());
		return xstream.toXML(musicMessage);
	}
	
	
	/**
	 * 关注公众号事件推送
	 * @param FromUserName
	 * @param ToUserName
	 * @param Content
	 * @return
	 */
	public static String initText(String FromUserName, String ToUserName, String Content){
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(FromUserName);
		textMessage.setFromUserName(ToUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.MESSAGE_TEXT);
		textMessage.setContent(Content);
		return textMessageToXml(textMessage);
	}
	
	
	/**
	 * 主菜单推送/回复
	 * @return
	 */
	public static String MenuText(){
		StringBuffer sb = new StringBuffer();
		sb.append(ReadFileUtil.readFile(ReadFileUtil.class.getClassLoader().getResource("../../TXT/welcome.txt").getPath()));
		return sb.toString();
	}
	
	
	/**
	 * 回复1事件文本消息
	 * @return
	 */
	public static String FirstText(){
		StringBuffer sb = new StringBuffer();
		sb.append(ReadFileUtil.readFile(ReadFileUtil.class.getClassLoader().getResource("../../TXT/Linux_cmd.txt").getPath()));
		return sb.toString();
	}
	// request.getSession().getServletContext().getRealPath("/") + "TXT/test.txt"
	
	
	/**
	 * 回复2事件文本消息
	 * @return
	 */
	public static String secondText(){
		StringBuffer sb = new StringBuffer();
		sb.append(ReadFileUtil.readFile(ReadFileUtil.class.getClassLoader().getResource("../../TXT/Solaris_cmd.txt").getPath()));
//		System.out.println("----->2" + sb.toString());
		return sb.toString();
	}
	
	
	/**
	 * 回复3事件文本消息
	 * @return
	 */
	public static String thirdText(){
		StringBuffer sb = new StringBuffer();
		sb.append(ReadFileUtil.readFile(ReadFileUtil.class.getClassLoader().getResource("../../TXT/common_cmd.txt").getPath()));
//		System.out.println(ReadFileUtil.class.getClassLoader().getResource("../../TXT/common_cmd.txt").getPath());
		return sb.toString();
	}
	
	
	/**
	 * 图文消息回复组装
	 * @param FromUserName
	 * @param ToUserName
	 * @return xml格式消息
	 */
	public static String initNews(String FromUserName, String ToUserName, List<News> news){
//		List<News> newslist = new ArrayList<News>();
//		News news = new News();
//		news.setTitle("vmstat命令详解");
//		news.setDescription("世界也可解释由可感知的");
//		news.setPicUrl("http://shengy.tunnel.qydev.com/Weixin/image/world.jpg");
//		news.setUrl("http://shengy.tunnel.qydev.com/Weixin/vmstat.html");
	
//		newslist.add(news);
//		newslist.add(news2);
		
		NewsMessage newsMessage = new NewsMessage();
		newsMessage.setToUserName(FromUserName);
		newsMessage.setFromUserName(ToUserName);
		newsMessage.setCreateTime(new Date().getTime());
		newsMessage.setMsgType(MESSAGE_NEWS);
		newsMessage.setArticleCount(news.size());
		newsMessage.setArticles(news);
		return newsMessageToXml(newsMessage);
	}
	
	
	/**
	 * 回复4事件图文消息 
	 * @return News对象
	 */
	public static News fourthNews(){
		News news = new News();
		news.setTitle("vmstat命令详解");
		news.setDescription("vmstat使用");
//		news.setPicUrl("http://shengy.tunnel.qydev.com/Weixin/image/background_vmstat.jpg");
//		news.setUrl("http://shengy.tunnel.qydev.com/Weixin/html/vmstat.html");
		news.setPicUrl("http://www.xuwangcheng.cn/weixin_shengy_test/image/background_vmstat.jpg");
		news.setUrl("http://www.xuwangcheng.cn/weixin_shengy_test/html/vmstat.html");
		return news;
	}
	
	
	/**
	 * 回复5事件图文消息
	 * @return
	 */
	public static News fifthNews(){
		News news = new News();
		news.setTitle("深入理解JVM");
		news.setDescription("深入理解Java Virtual Machine");
		news.setPicUrl("http://www.xuwangcheng.cn/weixin_shengy_test/image/background_jvm.jpg");
		news.setUrl("http://www.xuwangcheng.cn/weixin_shengy_test/html/jvm.html");
		return news;
	}
	
	/**
	 * 回复6事件图文消息
	 * @return
	 */
	public static News sixthNews(){
		News news = new News();
		news.setTitle("vi命令详解");
		news.setDescription("vi命令随用随查，简单快捷");
		news.setPicUrl("http://www.xuwangcheng.cn/weixin_shengy_test/image/background_vi.jpg");
		news.setUrl("http://www.xuwangcheng.cn/weixin_shengy_test/html/vi.html");
		return news;
	}
	
	
	/**
	 * 图片消息回复组装
	 * @param ToUserName
	 * @param FromUserName
	 * @param image
	 * @return
	 */
	public static String initImage(String FromUserName, String ToUserName, Image image){
		ImageMessage imageMessage = new ImageMessage();
		imageMessage.setFromUserName(ToUserName);
		imageMessage.setToUserName(FromUserName);
		imageMessage.setCreateTime(new Date().getTime());
		imageMessage.setMsgType(MESSAGE_IMAGE);
		imageMessage.setImage(image);
		return imageMessageToXml(imageMessage);
	}
	
	
	/**
	 * 回复7图片消息
	 * @return
	 */
	public static Image seventhImage(){
		Image image = new Image();
		try {
			image.setMediaId(WeixinUtil.upload(WeixinUtil.class.getClassLoader().getResource("../../resource/JG.png").getPath(), WeixinUtil.getAccessToken().getAccess_token(), MESSAGE_IMAGE));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}
	
	
	
	/**
	 * 回复9翻译文本消息
	 * @return
	 */
	public static String eighthTrans(){
		StringBuffer sb = new StringBuffer();
		sb.append(ReadFileUtil.readFile(ReadFileUtil.class.getClassLoader().getResource("../../TXT/Trans.txt").getPath()));
		return sb.toString();
	}
	
	
	/**
	 * 音乐消息回复组装
	 * @param FromUserName
	 * @param ToUserName
	 * @param music
	 * @return
	 */
	public static String initMusic(String FromUserName, String ToUserName, Music music){
		MusicMessage musicMessage = new MusicMessage();
		musicMessage.setToUserName(FromUserName);
		musicMessage.setFromUserName(ToUserName);
		musicMessage.setCreateTime(new Date().getTime());
		musicMessage.setMsgType(MESSAGE_MUSIC);
		musicMessage.setMusic(music);
		return musicMessageToXml(musicMessage);
	}
	
	
	/**
	 * 回复9音乐消息
	 * @return
	 * @throws IOException 
	 */
	public static Music ninethhMusic(HttpServletRequest req) throws IOException{
		Music music = new Music();
		String musicName = GetMusicNameUtil.getFileName(req);
		System.out.println("==========" + musicName);
		if(musicName.equals("qudali.mp3")){
			music.setTitle("去大理");
			music.setDescription("相遇洱海");
		}else if(musicName.equals("SeeYouAgain.mp3")){
			music.setTitle("See You Again");
			music.setDescription("再见，为以后");
		}else if(musicName.equals("lanlianhua.mp3")){
			music.setTitle("蓝莲花");
			music.setDescription("出淤泥不染，濯清涟不妖");
		}else if(musicName.equals("paomo.mp3")){
			music.setTitle("泡沫");
			music.setDescription("五彩泡沫，多彩人生");
		}else{
			music.setTitle("夜空中最亮的星");
			music.setDescription("一起奔跑，一起看星星");
		}
		music.setMusicUrl("http://www.xuwangcheng.cn/weixin_shengy_test/music/" + musicName);
		music.setHQMusicUrl("http://www.xuwangcheng.cn/weixin_shengy_test/music/" + musicName);
		try {
			music.setThumbMediaId(WeixinUtil.upload(WeixinUtil.class.getClassLoader().getResource("../../resource/m.jpg").getPath(), WeixinUtil.getAccessToken().getAccess_token(), MESSAGE_THUMB));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return music;
	}
	
	
	/**
	 * 语音消息回复组装
	 * @param FromUserName
	 * @param ToUserName
	 * @param voice
	 * @return
	 */
	public static String initVoice(String FromUserName, String ToUserName, Voice voice){
		VoiceMessage voiceMessage = new VoiceMessage();
		voiceMessage.setToUserName(FromUserName);
		voiceMessage.setFromUserName(ToUserName);
		voiceMessage.setCreateTime(new Date().getTime());
		voiceMessage.setMsgType(MESSAGE_VOICE);
		voiceMessage.setVoice(voice);
		return voiceMessageToXml(voiceMessage);
	}
	
	
	/**
	 * 回复10语音消息
	 * @return
	 */
	public static Voice tenthVoice(){
		Voice voice = new Voice();
		try {
			voice.setMediaId(WeixinUtil.upload(WeixinUtil.class.getClassLoader().getResource("../../resource/ly.mp3").getPath(), WeixinUtil.getAccessToken().getAccess_token(), MESSAGE_VOICE));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
		return voice;
	}



}
