package util;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import menu.Button;
import menu.ClickButton;
import menu.Menu;
import menu.ViewButton;
import net.sf.json.JSONObject;
import po.AccessToken;

/**
 * 微信工具类
 * @author shengyin
 *
 */
public class WeixinUtil {
	
	private static final String APPID = "自己的****";
	private static final String APPSECRET = "自己的****";
	private static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	private static final String CREATE_MENU_URL="https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	private static final String UPLOAD_URL="https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
	
	/**
	 * http/Get请求
	 * @param url
	 * @return 返回JSON数据包
	 */
	public static JSONObject doGetStr(String url){
		JSONObject jsonObject = null;
		
		HttpClientBuilder builder = HttpClientBuilder.create();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = builder.build().execute(httpGet);
			HttpEntity entity = response.getEntity();
			if(entity != null){
				String result = EntityUtils.toString(entity, "utf-8");
				jsonObject = JSONObject.fromObject(result);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return jsonObject;
	}
	
	/**
	 * http/post请求
	 * @param url
	 * @param outstr
	 * @return 返回JSON数据包
	 */
	public static JSONObject doPostStr(String url, String outstr){
		JSONObject jsonObject = null;
		
		HttpClientBuilder builder = HttpClientBuilder.create();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new StringEntity(outstr, "utf-8"));
		try {
			HttpResponse response = builder.build().execute(httpPost);
			HttpEntity entity = response.getEntity();
			jsonObject = JSONObject.fromObject(EntityUtils.toString(entity, "utf-8"));
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return jsonObject;
	}
	
	
	/**
	 * 文件上传
	 * @param filePath
	 * @param accessToken
	 * @param type
	 * @return
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws KeyManagementException
	 */
	public static String upload(String filePath, String accessToken,String type) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
		File file = new File(filePath);
		if (!file.exists() || !file.isFile()) {
			throw new IOException("文件不存在");
		}

		String url = UPLOAD_URL.replace("ACCESS_TOKEN", accessToken).replace("TYPE",type);
		
		URL urlObj = new URL(url);
		//连接
		HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();

		con.setRequestMethod("POST"); 
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setUseCaches(false); 

		//设置请求头信息
		con.setRequestProperty("Connection", "Keep-Alive");
		con.setRequestProperty("Charset", "UTF-8");

		//设置边界
		String BOUNDARY = "----------" + System.currentTimeMillis();
		con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);

		StringBuilder sb = new StringBuilder();
		sb.append("--");
		sb.append(BOUNDARY);
		sb.append("\r\n");
		sb.append("Content-Disposition: form-data;name=\"file\";filename=\"" + file.getName() + "\"\r\n");
		sb.append("Content-Type:application/octet-stream\r\n\r\n");

		byte[] head = sb.toString().getBytes("utf-8");

		//获得输出流
		OutputStream out = new DataOutputStream(con.getOutputStream());
		//输出表头
		out.write(head);

		//文件正文部分
		//把文件已流文件的方式 推入到url中
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		int bytes = 0;
		byte[] bufferOut = new byte[1024];
		while ((bytes = in.read(bufferOut)) != -1) {
			out.write(bufferOut, 0, bytes);
		}
		in.close();

		//结尾部分
		byte[] foot = ("\r\n--" + BOUNDARY + "--\r\n").getBytes("utf-8");//定义最后数据分隔线

		out.write(foot);

		out.flush();
		out.close();

		StringBuffer buffer = new StringBuffer();
		BufferedReader reader = null;
		String result = null;
		try {
			//定义BufferedReader输入流来读取URL的响应
			reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}
			if (result == null) {
				result = buffer.toString();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		JSONObject jsonObj = JSONObject.fromObject(result);
		System.out.println(jsonObj);
		String typeName = "media_id";
		if(!"image".equals(type) && !"voice".equals(type)){
			typeName = type + "_media_id";
		}
		String mediaId = jsonObj.getString(typeName);
		return mediaId;
	}
	
	
	/**
	 * 获取微信token值
	 * @return AccessToken
	 */
	public static AccessToken getAccessToken(){   
		AccessToken accessToken = new AccessToken();
		String url = ACCESS_TOKEN_URL.replace("APPID", APPID).replace("APPSECRET", APPSECRET);
		JSONObject jsonObject = doGetStr(url);
		if(jsonObject != null){
			accessToken.setAccess_token(jsonObject.getString("access_token"));
			accessToken.setExpires_in(jsonObject.getInt("expires_in"));
			accessToken.setSaveCurrentTime(System.currentTimeMillis() + "");
		}
		System.out.println("获取的：" + jsonObject);
		return accessToken;
	}
	
		
	/**
	 * 微信获取AccessToken并本地保存,判断时效,失效则重新取
	 * @param req
	 * @return token
	 * @throws IOException
	 */
	public static String getSaveAccessToken(HttpServletRequest request) throws IOException{
		System.out.println(request.getSession().getServletContext().getRealPath("/AccessToken")  + "/weixin_token.txt");
		File file = new File(request.getSession().getServletContext().getRealPath("/AccessToken")  + "/weixin_token.txt");
		String token = null;
		
		if(!file.exists()){
			System.out.println("文件不存在！");
		}
		if(file.length() == 0){
			AccessToken accessToken = WeixinUtil.getAccessToken();
			token = accessToken.getAccess_token();
			System.out.println("新增微信本地token：" + token);
			FileOutputStream fos = new FileOutputStream(file, false);//不允许追加URL xmlpath = this.getClass().getClassLoader().getResource("selected.txt");
			JSONObject json = JSONObject.fromObject(accessToken);
			String strjson = json.toString();
			fos.write(strjson.getBytes());
			fos.close();
		}else{
			FileInputStream fis = new FileInputStream(file);
			byte[] b = new byte[2048];
			int len = fis.read(b);
			String json_Access_Token = new String(b, 0, len);
			JSONObject jsonObject = JSONObject.fromObject(json_Access_Token);
			AccessToken accessToken = (AccessToken) JSONObject.toBean(jsonObject, AccessToken.class);
			long expires_in = accessToken.getExpires_in();
			if(accessToken.getSaveCurrentTime() != null){
				long saveTime = Long.parseLong(accessToken.getSaveCurrentTime());
				long nowTime = System.currentTimeMillis();
				long remiaTime = nowTime - saveTime;
				System.out.println("微信时间差" + remiaTime);
				if(remiaTime < (expires_in * 1000)){
					token = accessToken.getAccess_token();
					System.out.println("获取微信已保存的本地token:" + token);
				}else{
					AccessToken accessToken_2 = WeixinUtil.getAccessToken();
					token = accessToken_2.getAccess_token();
					System.out.println("失效微信从新获取token：" + token);
					FileOutputStream fos = new FileOutputStream(file, false);//不允许追加
					JSONObject json = JSONObject.fromObject(accessToken_2);
					String strjson = json.toString();
					fos.write(strjson.getBytes());
					fos.close();
				}
			}
			fis.close();
		}
		return token;
	}
	
	
	
//	public static void main(String[] args) throws IOException{
//		AccessToken accessToken = getAccessToken();
//		System.out.println("access_token" + accessToken.getAccess_token() + "\n" + "expires_in" + accessToken.getExpires_in());
//		getAccessToken();
//		getSaveAccessToken();
//	}
	

	/**
	 * 定义组装菜单
	 * @return
	 */
	public static Menu initMenu(){
		//Button button = new Button();
		ClickButton clickbutton11 = new ClickButton();
		clickbutton11.setName("查看主菜单");
		clickbutton11.setType("click");
		clickbutton11.setKey("11");
		
		ViewButton viewButton12 = new ViewButton();
		viewButton12.setName("百度一下");
		viewButton12.setType("view");
		viewButton12.setUrl("http://www.baidu.com");
		
		ClickButton clickbutton131 = new ClickButton();
		clickbutton131.setName("定位位置");
		clickbutton131.setType("location_select");
		clickbutton131.setKey("131");
		
		ClickButton clickbutton132 = new ClickButton();
		clickbutton132.setName("扫一扫");
		clickbutton132.setType("scancode_waitmsg");
		clickbutton132.setKey("132");
		
		Button button = new Button();
		button.setName("我的工具");
		button.setSub_button(new Button[]{clickbutton131,clickbutton132});
		
		Menu menu = new Menu();
		menu.setButton(new Button[]{clickbutton11,viewButton12,button});
		
		return menu;
	}
	
	
	/**
	 * 菜单创建
	 * 发起post请求获取errcode
	 * @param accessToken
	 * @param menu
	 * @return
	 */
	public static int createMenu(String accessToken, String menu){
		int ret = 0;
		String url = CREATE_MENU_URL.replace("ACCESS_TOKEN", accessToken);
		JSONObject jsonObject = doPostStr(url, menu);
		if(jsonObject != null){
			ret = jsonObject.getInt("errcode");
		}
		return ret;
		
	}
	
	
	
	

}
