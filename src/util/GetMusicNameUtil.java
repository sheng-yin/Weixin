package util;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

/**
 * 随机读取音乐文件类
 * @author shengyin
 */
public class GetMusicNameUtil {
	
//	public static final String MUSIC_PATH = "G:/testM";
	
	/**
	 * 随机读取音乐文件方法
	 * @param req
	 * @return 音乐名称
	 * @throws IOException
	 */
	public static String getFileName(HttpServletRequest req) throws IOException{
		File file = new File(req.getSession().getServletContext().getRealPath("/") + "music");
		if(!file.exists()){
			throw new IOException("文件不存在！");
		}
		
		File f[] = file.listFiles();
		Random ra =new Random(); 
		int a = ra.nextInt(f.length);
		File fs = f[a];
		return fs.getName();
		
	}
	
}
