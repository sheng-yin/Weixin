package baiduapi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import po.AccessToken;
import util.WeixinUtil;


/**
 * 百度API调用校验类
 * @author shengyin
 *
 */
public class BaiduCheckUtil {
	
	private static final String  VOICE_TOKEN_URL = "https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=API_KEY&client_secret=SECRET_KEY&";
	
	/**
	 * 随机生成4位数字
	 * @return
	 */
	public static int Ramnumber(){
		int ret;
		Random ra = new Random();
		ret = ra.nextInt(8999) + 1000;
		return ret;
	}
	

	/**
	 * MD5的32位加密算法
	 * @param str
	 * @return
	 */
	public static String MD5(String str){
		String ret = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(str.getBytes("utf-8"));
			byte[] b = md5.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
			    i = b[offset];
			    if (i < 0)
			      i += 256;
			    if (i < 16)
			      buf.append("0");
			    buf.append(Integer.toHexString(i));
			  }
			ret = buf.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return ret;
		
	}
	
	
	/**
	 * 返回MD5加密后百度翻译校验字符串Sign
	 * @param appid
	 * @param q
	 * @param salt
	 * @param screct
	 * @return
	 */
	public static String CheckToken(String appid, String q, String salt, String screct){
		String str = appid + q + salt + screct;
		String sign = MD5(str);
		System.out.println(sign);
		return sign;
	}
	
	
//***********************************************************************************************************
	
	/**
	 * 获取百度授权签名token
	 * @param apikey
	 * @param secretkey
	 * @return
	 */
	public static AccessToken getVoiceAccessToken(String apikey, String secretkey){
		AccessToken accessToken = new AccessToken();
		String url = VOICE_TOKEN_URL.replace("API_KEY", apikey).replace("SECRET_KEY", secretkey);
		JSONObject jsonObject = WeixinUtil.doGetStr(url);
		System.out.println("百度token: " + jsonObject);
		if(jsonObject != null){
			accessToken.setAccess_token(jsonObject.getString("access_token"));
			accessToken.setExpires_in(jsonObject.getLong("expires_in"));
			accessToken.setSaveCurrentTime(System.currentTimeMillis() + "");
		}
		
		return accessToken;
		
	}
	
	
	
	
	public static String getBaiduSaveToken(String apikey, String secretkey, HttpServletRequest req) throws IOException{
		File file = new File(req.getSession().getServletContext().getRealPath("/AccessToken") + "/baidu_token.txt");
		if(!file.exists()){
			System.out.println("百度Token文件不存在！");
		}
		String token = null;
		if(file.length() == 0){
			AccessToken accessToken = BaiduCheckUtil.getVoiceAccessToken(apikey, secretkey);
			token = accessToken.getAccess_token();
			System.out.println("新增百度语音本地token：" + token);
			FileOutputStream fos = new FileOutputStream(file, false);//不允许追加
			JSONObject jsonObject = JSONObject.fromObject(accessToken);
			String strjosn = jsonObject.toString();
			fos.write(strjosn.getBytes());
			fos.close();
		}else{
			FileInputStream fin = new FileInputStream(file);
			byte[] b = new byte[2048];
			int len = fin.read(b);
			String strjson = new String(b, 0, len);
			JSONObject jsonObject = JSONObject.fromObject(strjson);
			AccessToken accessToken = (AccessToken) JSONObject.toBean(jsonObject, AccessToken.class);
			if(accessToken.getSaveCurrentTime() != null){
				long expires_in = accessToken.getExpires_in();
				long saveTime = Long.parseLong(accessToken.getSaveCurrentTime());
				long nowTime = System.currentTimeMillis();
				long remiaTime = nowTime - saveTime;
				System.out.println("百度时间差" + remiaTime);
				if(remiaTime < (expires_in * 1000)){
					token = accessToken.getAccess_token();
					System.out.println("获取百度语音已保存的本地token:" + token);
				}else{
					AccessToken accessToken_2 = BaiduCheckUtil.getVoiceAccessToken(apikey, secretkey);
					token = accessToken_2.getAccess_token();
					System.out.println("失效百度语音从新获取token：" + token);
					FileOutputStream fos = new FileOutputStream(file, false);//不允许追加
					JSONObject jsonObject_2 = JSONObject.fromObject(accessToken_2);
					String strjosn = jsonObject_2.toString();
					fos.write(strjosn.getBytes());
					fos.close();
				}
				
			}
			fin.close();
		}
		return token;
	}
	
	
//	public static void main(String[] args){
//		getVoiceAccessToken(API_KEY, SECRET_KEY);
//	}
	
	

}
