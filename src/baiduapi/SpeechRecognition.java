package baiduapi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class SpeechRecognition {
	
	private static final String API_KEY = "自己的****";
	private static final String SECRET_KEY="自己的****";
	private static final String SERVERURL = "http://vop.baidu.com/server_api";  
	
	
	/**
	 * Base64编码
	 * @param bytes
	 * @return
	 */
	public static String encode(final byte[] bytes){
		return new String(Base64.encodeBase64(bytes));
	}
	
	
	/**
	 * 读取文件以字节数组输出
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] loadFile(File file) throws IOException{
		InputStream is = new FileInputStream(file);
		byte[] bytes = new byte[(int) file.length()];
		int offset = 0;  
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {  
            offset += numRead;  
        }  
        
        if (offset < bytes.length) {  
            is.close();  
            throw new IOException("Could not completely read file " + file.getName());  
        } 
        
        is.close();
        return bytes;
	}
	
	
	/**
	 * 打印结果
	 * @param conn
	 * @return
	 * @throws IOException
	 */
	public static String printResponse(HttpURLConnection conn) throws IOException{
		if(conn.getResponseCode() != 200){
			System.out.println("调用百度语音识别状态码为：" + conn.getResponseCode());
			return "sorry，http请求发送失败！";
		}
		System.out.println("调用百度语音识别状态码为：" + conn.getResponseCode());
		InputStream is = conn.getInputStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is,"UTF-8"));
		String line = null;
		StringBuffer response = new StringBuffer();
		while((line = rd.readLine()) != null){
//			String str = new String(line.getBytes(), "UTF-8");
			response.append(line);
			response.append("\r");
		}
		rd.close();
		System.out.println("调用百度语音识别返回结果：" + response.toString());
		JSONObject jsonObj = JSONObject.fromObject(response.toString());
		JSONArray jsonArray = jsonObj.getJSONArray("result");
//		System.out.println(jsonArray.getString(0));
		//buf.replace(buf.length() - 1, buf.length(), "")replace(jsonArray.getString(0).length() - 1, jsonArray.getString(0).length(), "");
		return jsonArray.getString(0).substring(0, jsonArray.getString(0).length() - 1);//str.substring(0,str.length()-1); 
	}
	
	
	
	/**
	 * 组装百度语音识别入参，发http请求，调用printResponse返回结果
	 * @return
	 * @throws IOException
	 */
	public static String jsonUpload(HttpServletRequest req) throws IOException{
		File filename  = new File(SpeechRecognition.class.getClassLoader().getResource("../../tmpVoice/test.amr").getPath());
		HttpURLConnection connection = (HttpURLConnection) new URL(SERVERURL).openConnection();
		
		JSONObject jsonparam = new JSONObject();
		jsonparam.put("format", "amr");
		jsonparam.put("rate", 8000);
		jsonparam.put("channel", 1);
		jsonparam.put("cuid", "shengyin");
		jsonparam.put("token", BaiduCheckUtil.getBaiduSaveToken(API_KEY, SECRET_KEY, req)); //调用BaiduCheckUtil类获取token
		jsonparam.put("lan", "zh");
		jsonparam.put("speech", encode(loadFile(filename)));  //读取文件以字节输出
		jsonparam.put("len", filename.length());
		
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
		connection.setDoOutput(true);
		connection.setDoInput(true);
		
		DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
		dataOutputStream.writeBytes(jsonparam.toString());
		System.out.println("拼接百度语音识别入参：" + jsonparam.toString());
		dataOutputStream.flush();
		dataOutputStream.close();
		
		return printResponse(connection);  //打印结果
	}
	
//	public static void main(String[] args) throws IOException{
//		jsonUpload();
//	}

}
