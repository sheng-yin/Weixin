package baiduapi;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import util.WeixinUtil;


/**
 * 百度翻译工具类
 * @author shengyin
 *
 */
public class TransLateUtil {
	
	private static final String  APPID = "自己的****";
	private static final String SECRECT = "自己的****";
	private static final String AUTOTOEN_URL = "http://api.fanyi.baidu.com/api/trans/vip/translate?q=KEYWORD&from=auto&to=en&appid=20171215000105298&salt=RAM&sign=SIGN";
	private static final String AUTOTOZH_URL = "http://api.fanyi.baidu.com/api/trans/vip/translate?q=KEYWORD&from=auto&to=zh&appid=20171215000105298&salt=RAM&sign=SIGN";
	
	/**
	 * 调用百度翻译API
	 * @param param
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String translateAutoToEnglish(String param) throws UnsupportedEncodingException{
		String strNO = String.valueOf(BaiduCheckUtil.Ramnumber());
		String sign = BaiduCheckUtil.CheckToken(APPID, param, strNO, SECRECT);
		String url = AUTOTOEN_URL.replace("KEYWORD", URLEncoder.encode(param, "utf-8")).replace("RAM", strNO).replace("SIGN", sign);
		JSONObject jsonObject = WeixinUtil.doGetStr(url);
		JSONArray jsonArray = jsonObject.getJSONArray("trans_result");
		JSONObject jsonObject2 = (JSONObject) jsonArray.get(0);
		String src = jsonObject2.getString("src");
		String dst = jsonObject2.getString("dst");
		System.out.println(jsonObject);
		String transStr = "原文：" + src + "\n" + "译文：" + dst;
		return transStr;
		
	}
	
	
	/**
	 * AUTO翻译成中文
	 * @param param
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String translateAutoToChinese(String param) throws UnsupportedEncodingException{
		String strNO = String.valueOf(BaiduCheckUtil.Ramnumber());
		String token = BaiduCheckUtil.CheckToken(APPID, param, strNO, SECRECT);
		String url = AUTOTOZH_URL.replace("KEYWORD", URLEncoder.encode(param, "utf-8")).replace("RAM", strNO).replace("SIGN", token);
		JSONObject jsonObject = WeixinUtil.doGetStr(url);
		JSONArray jsonArray = jsonObject.getJSONArray("trans_result");
		JSONObject jsonObject2 = (JSONObject) jsonArray.get(0);
		String src = jsonObject2.getString("src");
		String dst = jsonObject2.getString("dst");
		System.out.println(jsonObject);
		String transStr = "原文：" + src + "\n" + "译文：" + dst;
		return transStr;
	}
	
	
	/**
	 * AUTO翻译成英文,判断字符串是否为中文
	 * @param str
	 * @return
	 */
	public static boolean IsChineseOrNot(String str){
		String reg = "[\\u4e00-\\u9fa5]+";   //匹配汉字
		String string = str.replaceAll(reg, "");  //将汉字替换为空
		//boolean result = str.matches(reg);
		//System.out.println("========" + result);
		if(string.isEmpty()){
			return true;
		}
		return false;
	}
	
	
	
//	public static void main(String[] args){
//		System.out.println(IsChineseOrNot("你好，今晚一起吃饭"));
//	}


}
