package baiduapi;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import util.WeixinUtil;

/**
 * 功能 :下载临时语音素材类
 * @author shengyin
 */
public class DownLoadVoice {
	
	public static final String STRURL = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
	
	/**
	 * 使用微信token和素材media_id拼接语音素材下载URL
	 * @param media_id
	 * @return
	 * @throws IOException 
	 */
	public static String getVoiceUrl(String media_id, HttpServletRequest request) throws IOException{
//		AccessToken accesstoken = WeixinUtil.getAccessToken();
//		String token = accesstoken.getAccess_token();
		String token = WeixinUtil.getSaveAccessToken(request);
		String Url = STRURL.replace("ACCESS_TOKEN", token).replace("MEDIA_ID", media_id);
//		String url = URLEncoder.encode(StrUrl);
		return Url;
	}
	
	
	/**
	 * 下载weixin语音素材
	 * @param strUrl
	 * @param filename
	 * @throws MalformedURLException
	 */
	public static void downVoice(String media_id, HttpServletRequest request){
		int bytesum = 0;
		InputStream is = null;
		FileOutputStream fileOutputStream = null;
		try {
			URL urlget = new URL(getVoiceUrl(media_id, request));
			HttpURLConnection http = (HttpURLConnection) urlget.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			System.setProperty("sun.net.client.defaultConnectTimeout", "30000");
			System.setProperty("sun.net.client.defaultReadTimeout", "30000");
			http.connect();
			// 获取文件转化为byte流 
			is = http.getInputStream();
			byte[] data = new byte[10240]; 
			int len = 0;  
			fileOutputStream = new FileOutputStream(request.getSession().getServletContext().getRealPath("/tmpVoice")  + "/test.amr"); //下载后保存的工程目录
			while ((len = is.read(data)) != -1){
				fileOutputStream.write(data, 0, len);
				bytesum = bytesum + len;
			}
			System.out.println("微信语音下载，字节数为: " + bytesum);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fileOutputStream != null){
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		
	}
	
	
//	public static void main(String[] args){
//		String url = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=5_NeXEtGX8XiKfxnLPCZzJigrwWpMufz1IiCBLuVD51nQNUJpKf4v3_LFOk-RoBBaNkvXKkJ_Phx-buuw9iFlN9lInmmZLSgGIcsf7AXLlWamVx3qnSZgyoE5AIU0TCNhAFALUF&media_id=x7ef56GjY-iESIgZqbE-trHw8KwV4XWO9Xfo0okIdOt82jULHt2sLbKSzzHzK9Tn";
//		downVoice("x7ef56GjY-iESIgZqbE-trHw8KwV4XWO9Xfo0okIdOt82jULHt2sLbKSzzHzK9Tn");
//	}

}
