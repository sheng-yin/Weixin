package info;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import baiduapi.TransLateUtil;
import po.News;
import util.MessageUtil;

/**
 * 微信被动回复消息逻辑类
 * @author shengyin
 *
 */
public class ReplyInfoSet {
	
	/**
	 * text类型回复逻辑
	 * @param Content
	 * @param req
	 * @param FromUserName
	 * @param ToUserName
	 * @return
	 * @throws IOException
	 */
	public static String ReplyTextInfo(String MsgType, String Content, HttpServletRequest req, String FromUserName, String ToUserName) throws IOException{
		String message = null;
		if("1".equals(Content) || "一".equals(Content)){
			message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.FirstText());
		}else if("2".equals(Content) || "二".equals(Content)){
			message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.secondText());
		}else if("3".equals(Content) || "三".equals(Content)){  
			message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.thirdText());
		}else if("?".equals(Content) || "？".equals(Content)){
			message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.MenuText());
		}else if("4".equals(Content) || "四".equals(Content)){
			List<News> news = new ArrayList<News>();
			news.add(MessageUtil.fourthNews());
			message = MessageUtil.initNews(FromUserName, ToUserName, news);
		}else if("5".equals(Content) || "五".equals(Content)){
			List<News> news = new ArrayList<News>();
			news.add(MessageUtil.fifthNews());
			message = MessageUtil.initNews(FromUserName, ToUserName, news);
		}else if("6".equals(Content) || "六".equals(Content)){
			List<News> news = new ArrayList<News>();
			news.add(MessageUtil.sixthNews());
			message = MessageUtil.initNews(FromUserName, ToUserName, news);
		}else if("7".equals(Content) || "七".equals(Content)){
			message = MessageUtil.initImage(FromUserName, ToUserName, MessageUtil.seventhImage());
		}else if("8".equals(Content) || "八".equals(Content)){
			message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.eighthTrans());
		}else if("9".equals(Content) || "九".equals(Content)){
			message = MessageUtil.initMusic(FromUserName, ToUserName, MessageUtil.ninethhMusic(req));
		}else if("10".equals(Content) || "十".equals(Content)){
			message = MessageUtil.initVoice(FromUserName, ToUserName, MessageUtil.tenthVoice());
		}else if(Content.startsWith("翻译")){
//			String str = Content.replace("翻译", "").replace(" ", "").trim();
			String str = Content.replace("翻译", "");
			boolean ret = TransLateUtil.IsChineseOrNot(str);
			if("".equals(str)){
				message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.eighthTrans());
			}else if(ret == false){
				message = MessageUtil.initText(FromUserName, ToUserName, TransLateUtil.translateAutoToChinese(str));
			}else{
				message = MessageUtil.initText(FromUserName, ToUserName, TransLateUtil.translateAutoToEnglish(str));
			}
		}else{
			if(MessageUtil.MESSAGE_VOICE.equals(MsgType)){
				message = MessageUtil.initText(FromUserName, ToUserName, "你的语音为: --->" + Content + "\n\n" + MessageUtil.MenuText());
			}else{
				message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.MenuText());
			}
			
		}
			
		return message;
	}
	
	
	/**
	 * Event类型回复逻辑
	 * @param Event
	 * @param FromUserName
	 * @param ToUserName
	 * @param ScanCodeInfo
	 * @param ScanType
	 * @param ScanResult
	 * @return
	 */
	public static String ReplyEventInfo(String Event, String FromUserName, String ToUserName, String ScanCodeInfo, String ScanType, String ScanResult){
		String message = null;
		if(MessageUtil.MESSAGE_SUBSCRIBE.equals(Event)){
			message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.MenuText());
		}else if(MessageUtil.MESSAGE_CLICK.equals(Event)){
			message = MessageUtil.initText(FromUserName, ToUserName, MessageUtil.MenuText());
		}else if(MessageUtil.MESSAGE_SCANCODE_WAITMSG.equals(Event)){
			String info = "扫描信息：" + ScanCodeInfo +"\n" + "扫描类型：" + ScanType + "\n" + "扫描结果：" + ScanResult;
			message = MessageUtil.initText(FromUserName, ToUserName, info);
		}
		return message;
	}
	
	
	/**
	 * Location类型回复逻辑
	 * @param FromUserName
	 * @param ToUserName
	 * @param Location_X
	 * @param Location_Y
	 * @param Label
	 * @return
	 */
	public static String ReplyLocationInfo(String FromUserName, String ToUserName, String Location_X, String Location_Y, String Label){
		String message = null;
		String info = "纬度：" + Location_X +"\n" + "经度：" + Location_Y + "\n" + Label;
		message = MessageUtil.initText(FromUserName, ToUserName, info);
		return message;
	}
	
	

}
