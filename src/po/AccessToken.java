package po;

public class AccessToken {
	
	private String access_token;
	private long expires_in;
	private String saveCurrentTime;
	

	public String getSaveCurrentTime() {
		return saveCurrentTime;
	}
	public void setSaveCurrentTime(String saveCurrentTime) {
		this.saveCurrentTime = saveCurrentTime;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public long getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}
	
	

}
