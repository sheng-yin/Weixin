package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.DocumentException;

import baiduapi.DownLoadVoice;
import baiduapi.SpeechRecognition;
import info.ReplyInfoSet;
import util.CheckUtil;
import util.MessageUtil;

public class WeixinServlet extends HttpServlet {

	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 接入校验
	*/
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String signature = req.getParameter("signature");
		String timestamp = req.getParameter("timestamp");
		String nonce = req.getParameter("nonce");
		String echostr = req.getParameter("echostr");
		
		PrintWriter out = resp.getWriter();
		if(CheckUtil.checkSignature(signature, timestamp, nonce)){
			out.print(echostr);
		}
	}
	
	/**
	 * 消息接受与响应
	 * ①接受XML转换成Map集合
	 * ②响应转换成XML
	 */
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//设置接受与响应编码
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		
		//创建输出流对象
		PrintWriter out = resp.getWriter();
		try {
			Map<String, String> map = MessageUtil.xmlToMap(req);
			String ToUserName = map.get("ToUserName");
			String FromUserName = map.get("FromUserName");
			String MsgType = map.get("MsgType");
			
			String message = null;
			if(MessageUtil.MESSAGE_TEXT.equals(MsgType)){
				String Content = map.get("Content");
				message = ReplyInfoSet.ReplyTextInfo(MsgType, Content ,req, FromUserName, ToUserName);
				//----
			}else if(MessageUtil.MESSAGE_EVENT.equals(MsgType)){
				String Event = map.get("Event");
				String ScanCodeInfo = map.get("ScanCodeInfo");
				String ScanType = map.get("ScanType");
				String ScanResult = map.get("ScanResult");
				message = ReplyInfoSet.ReplyEventInfo(Event, FromUserName, ToUserName, ScanCodeInfo, ScanType, ScanResult);
				//--
			}else if(MessageUtil.MESSAGE_LOCATION.equals(MsgType)){
				String Location_X = map.get("Location_X");
				String Location_Y = map.get("Location_Y");
				String Label = map.get("Label");
				message = ReplyInfoSet.ReplyLocationInfo(FromUserName, ToUserName, Location_X, Location_Y, Label);
				//--
			}else if(MessageUtil.MESSAGE_VOICE.equals(MsgType)){
				String MediaId = map.get("MediaId");
//				System.out.println(MediaId);
				DownLoadVoice.downVoice(MediaId, req);
				String Content = SpeechRecognition.jsonUpload(req);
				message = ReplyInfoSet.ReplyTextInfo(MsgType, Content ,req, FromUserName, ToUserName);
			}
			
			System.out.println(message);
			out.print(message);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			out.close();
		}
		
	}
	


}
