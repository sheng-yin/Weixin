# 基于java的易于上手的微信公众号开发测试号

#### 介绍
基于java的易于上手的微信公众号开发测试号

#### 功能说明

1. 基于菜单列表的信息查询
2. 基于百度API的语音识别
3. 基于百度API的中英文互译等

#### 开发环境
1. JDK >= 1.7
2. ide: Eclipse

#### 使用说明

1. 填写自己的微信公众号APPID
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/0318/172955_ad5d2869_1759199.png "微信图片_20190318165040.png")
2. 填写自己的百度语音识别APP_KEY
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/0318/173005_edf725e6_1759199.png "微信图片_20190318165547.png")
3. 填写自己的百度翻译APPID
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/0318/173015_1014ee1d_1759199.png "微信图片_20190318165837.png")
4. 微信公众平台接口信息配置
    ![输入图片说明](https://images.gitee.com/uploads/images/2019/0318/173735_36cef0e1_1759199.png "微信图片_20190318173607.png")

#### 体验

